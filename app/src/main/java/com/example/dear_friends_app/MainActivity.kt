package com.example.dear_friends_app

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.Menu
import androidx.appcompat.widget.SearchView
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.StaggeredGridLayoutManager
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity(){  //,android.widget.SearchView.OnQueryTextListener {

    var allfriends=ArrayList<Friends>()
   lateinit var myAdapter:FriendAdapter // lateinit yapmamiz gerek  basta degilde sonradan deger verecegim anlaminda
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        dataSource()


         myAdapter=FriendAdapter(allfriends)
        recyclerViewFriends.adapter=myAdapter


        var LayoutManager= StaggeredGridLayoutManager(2,StaggeredGridLayoutManager.VERTICAL) //true yaparsan tersten listelenir
        recyclerViewFriends.layoutManager=LayoutManager

        //Textview birsey yazildiginda dinleyecek sinif(ama abstract anonim class lardan yararlanan)
        searchViewFriend.setOnQueryTextListener(object : SearchView.OnQueryTextListener{
            override fun onQueryTextSubmit(query: String?): Boolean {
               //Enter basildiginda  dinle
                return false
            }

            override fun onQueryTextChange(newText: String?): Boolean {
              //her bir karakter girildiginde tetiklenecek meethid

                myAdapter.filter.filter(newText)

                return false //neden false biz listiner seklinde yapmadik
            }

        })


    }

    //Menu olusturuyoruz
   /* override fun onCreateOptionsMenu(menu: Menu?): Boolean {

        menuInflater.inflate(R.menu.filter_menu,menu)

        var searchItem=menu?.findItem(R.id.app_bar_search)

        var searchView=searchItem?.actionView as android.widget.SearchView
        searchView.setOnQueryTextListener(this)

        return super.onCreateOptionsMenu(menu)
    }*/

    private fun dataSource() {

        var allImg= arrayOf(R.drawable.ani_cat_one,R.drawable.ani_cat_two,R.drawable.ani_cat_three,R.drawable.ani_cat_four,R.drawable.ani_cat_five,R.drawable.ani_cat_six,R.drawable.ani_cat_seven,

            R.drawable.ani_dog_one,R.drawable.ani_dog_two,R.drawable.ani_dog_three,R.drawable.ani_dog_four,R.drawable.ani_dog_five,
            R.drawable.ani_deer_one,R.drawable.ani_deer_two,R.drawable.ani_deer_three,R.drawable.ani_deer_four,
            R.drawable.bird_parrot_one,R.drawable.bird_parrot_two,R.drawable.bird_parrot_three,R.drawable.bird_parrot_four,R.drawable.bird_parrot_five,R.drawable.bird_parrot_six,R.drawable.bird_parrot_seven,R.drawable.bird_parrot_eight

            )

        var names= arrayOf("Kedi 1","Kedi 2","Kedi 3","Kedi 4","Kedi 5","Kedi 6","Kedi 7",
            "Köpek 1","Köpek 2","Köpek 3","Köpek 4","Köpek 5",
            "Geyik 1","Geyik 2","Geyik 3","Geyik 4",
            "Papağan 1","Papağan 2","Papağan 3","Papağan 4","Papağan 5","Papağan 6","Papağan 7","Papağan 8"
            )


        for(i in 0..allImg.size-1){
            var addFriends=Friends(names[i],allImg[i])
            allfriends.add(addFriends)
        }


    }

    /*
    override fun onQueryTextSubmit(query: String?): Boolean { //enter yada bir butona tikladiginizda arama islemini yapar
        return false
    }

    override fun onQueryTextChange(newText: String?): Boolean {
        var tempData=newText?.toLowerCase() //girilen yazileri kucuk harf cevirdik
        var wanteds=ArrayList<Friends>()

    //adapterin bu wanteds dan bir haberi yok bunu adaptere gondermek gerekiyor
        for(i in allfriends ){
            var name=i.name.toLowerCase()
            if(name.contains(tempData.toString())){
                wanteds.add(i)
            }
        }

        myAdapter.setFilter(wanteds) //arananlari adaptera gonderdik

        return true
    }

     */


}
