package com.example.dear_friends_app

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.CountDownTimer

class SplashActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)
    }

    //neden onResume --> kullanici mainactivity  gectikten sonra tekrar splash ekranına geldiginde mainactivity gidemez(ONcreate olsaydi)
    override fun onResume() {

        object :CountDownTimer(3000,1000){
            override fun onFinish() { //3 sn sonunda

                var intent= Intent(this@SplashActivity,MainActivity::class.java)
                startActivity(intent)
            }

            override fun onTick(millisUntilFinished: Long) { //her 1sn bir

            }



        }.start()


        super.onResume()
    }
}
