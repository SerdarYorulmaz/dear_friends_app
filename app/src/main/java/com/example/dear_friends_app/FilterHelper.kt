package com.example.dear_friends_app

import android.widget.Filter

class FilterHelper(allFriend:ArrayList<Friends>,adapter: FriendAdapter):Filter() { //burda adapter nesnesinide gonderdik bir sekilde adapter erismemiz gerekiyor burda erisemiyoruz adpter filter bildirmemiz gerek

    var currentAdapter=adapter
    var currentListe=allFriend

    override fun performFiltering(constraint: CharSequence?): FilterResults {
        //filtereleme yapacak method burasi

        var result=FilterResults()

        //constraint --> kullanicinin aramada yazdigi degerler

        if(constraint!=null && constraint.length>0){

            var wantedName=constraint.toString().toLowerCase()

            var matchings=ArrayList<Friends>() //eslesen deger gore liste yenielecek

            for (i in currentListe){

                var name=i.name.toString().toLowerCase() //aranılacak olan ile aranan degerleri toLowerCase kucult yoksa dogru degerler bulamayabilirsin
                if(name.contains(wantedName.toString())){
                    matchings.add(i)
                }
            }

            result.values=matchings
            result.count=matchings.size
        }else{
            result.values=currentListe
            result.count=currentListe.size
        }

        return result

    }

    override fun publishResults(constraint: CharSequence?, results: FilterResults?) {

        //filtereleme gosterilmesi,adapter guncellemesi burda yapilacak
            //RESULT adapter gondermemiz gerekiyor

        currentAdapter.setFilterA(results?.values as ArrayList<Friends>)
        currentAdapter.notifyDataSetChanged()


    }

}