package com.example.dear_friends_app

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_detail.*
import kotlinx.android.synthetic.main.single_member.*

class DetailActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail)

        var inComingIntent=intent
        if(inComingIntent!=null){
            txtDetail.setText(inComingIntent.getStringExtra("name"))
            imgDetail.setImageResource(inComingIntent.getIntExtra("image",R.drawable.ani_cat_six))
        }
    }
}
