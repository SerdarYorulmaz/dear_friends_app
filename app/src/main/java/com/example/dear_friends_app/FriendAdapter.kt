package com.example.dear_friends_app

import android.content.Intent
import android.telecom.Call
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Filter
import android.widget.Filterable
import android.widget.Toast
import androidx.cardview.widget.CardView
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.single_member.view.*


class FriendAdapter(allFriends: ArrayList<Friends>) : RecyclerView.Adapter<FriendAdapter.ViewHolder>(),Filterable {
    var dearFriends = allFriends
    var myFilter=FilterHelper(allFriends,this) //adapter nesnesinin icindeyiz onun icin this


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {

        var inflater = LayoutInflater.from(parent.context)
        var onlyFriend=inflater.inflate(R.layout.single_member,parent,false)

        return ViewHolder(onlyFriend)

    }

    override fun getItemCount(): Int {
        return dearFriends.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        var tempData=dearFriends[position]
    holder.setData(tempData,position)
    }
    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) { //FriendAdapter class olusuturuken ViewHolder class iç class olarak eklemesin


        var onlyFriend=itemView as CardView


        var friendTitle=onlyFriend.txtFriendName


        var friendImg=onlyFriend.imgFriend

        fun setData(currentFriends: Friends,position: Int) {
            friendTitle.setText(currentFriends.name)
            friendImg.setImageResource(currentFriends.img)

            onlyFriend.setOnClickListener {v ->
                //context direk erisemiyoruz onun icin view den almamiz gerekiyor
                Toast.makeText(onlyFriend.context,"Tiklanan Oge: ${position}  Adı:${currentFriends.name}",Toast.LENGTH_LONG).show()
                   var intent= Intent(v.context,DetailActivity::class.java)
                intent.putExtra("name",currentFriends.name) //burdaki name stringi biribir diger activtiyde ayni olmak zorunda o reeransa gore aliniyor
                intent.putExtra("image",currentFriends.img)
                    v.context.startActivity(intent)
            }

            //yukardaki daha kisa ve ordaki v -->View den gelen v dir
            /*
            onlyFriend.setOnClickListener(object : View.OnClickListener{
                override fun onClick(v: View?) {

                }

            })

             */
        }

    }

/*
    fun setFilter(wanteds:ArrayList<Friends>){
        dearFriends=ArrayList<Friends>() //arraylistimizi bosaltiyoruz(sadece arananlar gelsin :))

        dearFriends.addAll(wanteds)

        //elimizde position bir deger olmadiginda kullanaviliriz notifyDataSetChanged()
        notifyDataSetChanged()


    }

 */

    fun setFilterA(wanteds:ArrayList<Friends>){

        dearFriends=wanteds //wanteds nesnesi filitrelenmis sonucları içerir.
    }

    override fun getFilter(): Filter { //olusturudugum filitreleme sinifini bana ver diyor

        return myFilter //adapter sınıfı icinde olsututurlan  filter tipindeki nesneyi donudurur
    }

}
